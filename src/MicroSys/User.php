<?php

namespace cdhpw\CloudLibrary\MicroSys;

use cdhpw\CloudLibrary\OpenAccount\Sign;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use stlswm\MicroserviceAssistant\ApiIO\ErrCode;
use stlswm\MicroserviceAssistant\ApiIO\IO;
use stlswm\MicroserviceAssistant\Cluster\System;

class User
{
    /**
     * 管理员登入状态与权限验证
     * @param  Request  $request
     * @param  Closure  $next
     * @param  string   $systemPrefix
     * @return mixed
     * @throws Exception
     */
    public static function authOperatorForMiddleware(Request $request, Closure $next, string $systemPrefix)
    {
        $token = $request->header("Access-Token");
        $enterpriseId = (int)$request->header('Enterprise-ID');
        if (!$token) {
            exit(response()->json(IO::fail(ErrCode::TokenErr, "请先登录！"))->content());
        }
        if (empty($enterpriseId)) {
            exit(response()->json(IO::fail(ErrCode::Business, "请选择企业！"))->content());
        }
        $router = $request->route()->getAction()['controller'];
        $router = str_replace("App\\Http\\Controllers\\", $systemPrefix, $router);
        $router = str_replace("\\", '/', $router);
        $api = '/api/server/operator/validate-and-check-power';
        ['code' => $code, 'data' => $operator, 'msg' => $msg] = System::innerRequest('user', $api, [
            'token'        => $token,
            'enterpriseId' => $enterpriseId,
            'router'       => $router
        ]);
        if ($code != 0) {
            exit(response()->json(IO::fail($code, $msg))->content());
        }
        $request->offsetSet('session', [
            'operator'   => [
                'id' => $operator['operatorId'],
            ],
            'account'    => [
                'id' => $operator['accountId'],
            ],
            'enterprise' => [
                'id' => $operator['enterpriseId']
            ],
        ]);
        return $next($request);
    }

    /**
     * 验证开放平台账号签名
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     * @throws Exception
     */
    public static function validateOpenAccountSign(Request $request, Closure $next)
    {
        $appId = (string)$request->input('appid');
        $nonceStr = (string)$request->input('nonce_str');
        $sign = (string)$request->input('sign');
        if (!$appId) {
            exit(response()->json(IO::fail(ErrCode::ParamEmpty, "应用id不能为空"))->content());
        }
        if (!$nonceStr || strlen($nonceStr) != 32) {
            exit(response()->json(IO::fail(ErrCode::ParamEmpty, "随机字符串必须并且只能是32位"))->content());
        }
        ['code' => $code, 'data' => $openAccount, 'msg' => $msg] = System::innerRequest('user',
            '/api/server/open-account/get-account', [
                'app_id' => $appId,
            ]);
        if ($code != 0) {
            exit(response()->json(IO::fail($code, $msg))->content());
        }
        $data = json_decode(file_get_contents('php://input'), true);
        unset($data['sign']);
        if (!Sign::checkSign($data, $openAccount['app_key'], $sign)) {
            Log::emergency("开放账号（{$appId}）签名错误".var_export($data, true).var_export($request->input(), true));
            exit(response()->json(IO::fail(ErrCode::TokenErr, "签名错误"))->content());
        }
        $request->offsetSet('session', [
            'openAccount' => $openAccount,
        ]);
        return $next($request);
    }

    /**
     * 验证开放平台账号token
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     * @throws Exception
     */
    public static function validateOpenAccountToken(Request $request, Closure $next)
    {
        ['code' => $code, 'data' => $openAccount, 'msg' => $msg] = System::innerRequest('user',
            '/api/server/open-account/validate-token', [
                'access_token' => $request->header('token'),
            ]);
        if ($code != 0) {
            exit(response()->json(IO::fail($code, $msg))->content());
        }
        $request->offsetSet('session', [
            'openAccount' => $openAccount,
        ]);
        return $next($request);
    }
}