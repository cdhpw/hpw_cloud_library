<?php

namespace cdhpw\CloudLibrary\MicroSys;

use Closure;
use Exception;
use Illuminate\Http\Request;
use stlswm\MicroserviceAssistant\ApiIO\ErrCode;
use stlswm\MicroserviceAssistant\ApiIO\IO;
use stlswm\MicroserviceAssistant\Cluster\System;

/**
 * Class Merchant
 * @package cdhpw\CloudLibrary\MicroSys
 */
class Merchant
{
    /**
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     * @throws Exception
     */
    public static function loginCheck(Request $request, Closure $next)
    {
        $token = $request->header('Account-Access-Token');
        if (!$token) {
            die(response()->json(IO::fail(ErrCode::TokenErr, "请先登录"))->content());
        }
        $api = '/api/server/account-token/validate-access-token';
        ['code' => $code, 'data' => $account, 'msg' => $msg] = System::innerRequest('user', $api, [
            'token' => $token
        ]);
        if ($code != 0) {
            die(response()->json(IO::fail($code, $msg))->content());
        }
        //商户验证
        ['code' => $code, 'data' => $merchant, 'msg' => $msg] = System::innerRequest('merchant',
            '/api/server/auth/merchant-login-check-v2', [
                'account_id'              => $account['id'],
                'merchant_id'             => $request->header('Merchant-Id'),
                'merchant_sub_account_id' => $request->header('Merchant-Sub-Account-Id'),
            ]);
        if ($code != 0) {
            die(response()->json(IO::fail($code, $msg))->content());
        }
        $request->offsetSet('session', [
            'account'            => $account,
            'merchant'           => $merchant['merchant'],
            'merchantSubAccount' => $merchant['merchant_sub_account'] ?? null,
        ]);
        return $next($request);
    }
}