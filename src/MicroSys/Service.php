<?php

namespace cdhpw\CloudLibrary\MicroSys;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use stlswm\MicroserviceAssistant\ApiIO\ErrCode;
use stlswm\MicroserviceAssistant\ApiIO\IO;
use stlswm\MicroserviceAssistant\Cluster\System;

/**
 * Class Service
 * @package cdhpw\CloudLibrary\MicroSys
 */
class Service
{
    /**
     * Laravel框架下
     * 微服务服务发现与缓存
     * @param  string  $redisConnection
     * @throws Exception
     */
    public static function discovery(string $redisConnection = 'hpw_cloud_gateway')
    {
        if (config('app.clusterKey')) {
            System::setClusterKey(config('app.clusterKey'));
        }
        $clusterSafeIps = config('app.clusterSafeIps');
        if ($clusterSafeIps) {
            System::$clusterIP = array_merge(System::$clusterIP, explode(',', $clusterSafeIps));
        }
        $redis = Redis::connection($redisConnection);
        $registeredSystem = $redis->get('RegisteredSystem');
        if (!$registeredSystem) {
            Log::emergency('获取微服务系统配置异常Redis Cache为空');
        } else {
            $registeredSystem = json_decode($registeredSystem, true);
            foreach ($registeredSystem as $systemAlias => $systemItem) {
                if (isset($systemItem[0])) {
                    $firstSystemItem = $systemItem[0];
                    System::addSystem($systemAlias, $firstSystemItem['protocol'].'://'.
                        $firstSystemItem['domain'].':'.$firstSystemItem['port'].$firstSystemItem['router']);
                }
            }
        }
    }

    /**
     * 内部请求检查
     * @param  Request  $request
     */
    public static function innerReqCheck(Request $request)
    {
        $remoteIP = $request->getClientIp();
        $authStr = (string)$request->header('Cluster-Auth');
        $random = (string)$request->header('Cluster-Random');
        $timestamp = (int)$request->header('Cluster-Timestamp');
        if (!System::isInnerReq($remoteIP, $authStr, $random, $timestamp)) {
            exit(response()->json(IO::fail(ErrCode::Business, "非法请求"))->content());
        }
    }
}