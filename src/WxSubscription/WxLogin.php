<?php

namespace cdhpw\CloudLibrary\WxSubscription;

/**
 * 公众号授权登陆
 * 加密与解密依赖laravel
 * Class WxLogin
 * @package cdhpw\CloudLibrary\WxSubscription
 */
class WxLogin
{
    /**
     * 微信Code登陆
     * @param  string  $appId
     * @param  string  $openId
     * @return string
     */
    public static function generatorWxCodeAccessToken(string $appId, string $openId): string
    {
        $authData = [
            'app_id'    => $appId,
            'open_id'   => $openId,
            'expiry_in' => time() + 86400 * 30,
        ];
        $accessToken = json_encode($authData);
        $accessToken = encrypt($accessToken);
        $accessToken = base64_encode($accessToken);
        return urlencode($accessToken);
    }

    /**
     * 微信Code登陆验证
     * @param  string  $accessToken
     * @return array
     */
    public static function validateWxCodeAccessToken(string $accessToken): array
    {
        if (empty($accessToken)) {
            return [false, "Token为空"];
        }
        try {
            $accessToken = urldecode($accessToken);
            $accessToken = base64_decode($accessToken);
            $accessToken = decrypt($accessToken);
            if (!$accessToken) {
                return [false, "无法解析Token[1]"];
            }
            $accessToken = json_decode($accessToken, true);
            if (!$accessToken) {
                return [false, "无法解析Token[2]"];
            }
            if ($accessToken['expiry_in'] < time()) {
                return [false, "登录已过期请重新登录"];
            }
            return [true, $accessToken];
        } catch (Exception $e) {
            return [false, $e->getMessage()];
        }
    }
}