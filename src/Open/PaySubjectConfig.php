<?php

namespace cdhpw\CloudLibrary\Open;

class PaySubjectConfig
{
    public int    $id;
    public string $subject;
    public string $alias;

    public int    $payAccountId;
    public string $payAccountAlias;
    public string $payAccountName;

    public int    $collectionCompanyId;
    public int    $collectionEnterpriseId;
    public string $collectionCompanyName;

    public int    $invoicingCompanyId;
    public int    $invoicingEnterpriseId;
    public string $invoicingCompanyName;
}