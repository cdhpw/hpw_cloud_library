<?php

namespace cdhpw\CloudLibrary\Open;

use Exception;
use Illuminate\Support\Facades\Redis;

class PaySubject
{


    /**
     * 获取支付配置
     * open项目redisConnection使用default
     * @param  string  $subjectCode
     * @param  string  $redisConnection
     * @return PaySubjectConfig
     * @throws Exception
     */
    public static function getConfig(string $subjectCode, string $redisConnection = 'hpw_cloud_open'): PaySubjectConfig
    {
        $redis = Redis::connection($redisConnection);
        $key = 'PAY_SUBJECT_'.$subjectCode;
        $config = $redis->get($key);
        if (!$config) {
            throw new Exception('获取收款配置时失败：'.$subjectCode);
        }
        $config = json_decode($config, true);
        $paySubjectConfig = new PaySubjectConfig();
        $paySubjectConfig->id = $config['id'];
        $paySubjectConfig->subject = $config['subject'];
        $paySubjectConfig->alias = $config['alias'];
        $paySubjectConfig->payAccountId = $config['pay_account']['id'];
        $paySubjectConfig->payAccountAlias = $config['pay_account']['alias'];
        $paySubjectConfig->payAccountName = $config['pay_account']['name'];
        $paySubjectConfig->collectionCompanyId = $config['company_id'];
        $paySubjectConfig->collectionEnterpriseId = $config['company']['enterprise_id'];
        $paySubjectConfig->collectionCompanyName = $config['company_name'];
        $paySubjectConfig->invoicingCompanyId = $config['invoice_company_id'];
        $paySubjectConfig->invoicingEnterpriseId = $config['invoice']['enterprise_id'];
        $paySubjectConfig->invoicingCompanyName = $config['invoice_company_name'];
        return $paySubjectConfig;
    }

}