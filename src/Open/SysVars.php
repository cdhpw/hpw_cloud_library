<?php

namespace cdhpw\CloudLibrary\Open;

use Exception;
use Illuminate\Support\Facades\Redis;

class SysVars
{
    /**
     * 获取系统变量
     * @param  string  $key
     * @param  string  $redisConnection
     * @return mixed
     * @throws Exception
     */
    public static function getVar(string $key, string $redisConnection = 'hpw_cloud_open')
    {
        $redis = Redis::connection($redisConnection);
        $key = 'SysVar:'.$key;
        $value = $redis->get($key);
        if (!$value) {
            throw new Exception('不存在的系统变量：'.$key);
        }
        return unserialize($value);
    }

}