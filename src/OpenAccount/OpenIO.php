<?php

namespace cdhpw\CloudLibrary\OpenAccount;

use Closure;
use Exception;
use Illuminate\Http\Request;
use stlswm\MicroserviceAssistant\ApiIO\ErrCode;
use stlswm\MicroserviceAssistant\Cluster\System;

class OpenIO
{
    /**
     * @param  Request  $request
     * @param  Closure  $next
     * @param  string   $systemPrefix
     * @return mixed
     * @throws Exception
     */
    public static function requestVerify(Request $request, Closure $next, string $systemPrefix)
    {
        $appId = (string)$request->input('appid');
        $nonceStr = (string)$request->input('nonce_str');
        $reqBizJson = (string)$request->input('req_biz_json');
        $sign = (string)$request->input('sign');
        $router = $request->route()->getAction()['controller'];
        $router = str_replace("App\\Http\\Controllers\\", $systemPrefix, $router);
        $router = str_replace("\\", '/', $router);
        ['code' => $code, 'data' => $openAccount, 'msg' => $msg] = System::innerRequest('user',
            '/api/server/open-account/get-account-and-validate-access-power', [
                'app_id' => $appId,
                'router' => $router,
            ]);
        if ($code != 0) {
            exit(response()->json(self::fail($openAccount ? $openAccount['app_key'] : '', $code, $msg))->content());
        }
        $checkData = [
            'appid'        => $appId,
            'nonce_str'    => $nonceStr,
            'req_biz_json' => $reqBizJson,
        ];
        if (config('app.env') == 'production') {
            if (!Sign::checkSign($checkData, $openAccount['app_key'], $sign)) {
                exit(response()->json(self::fail($openAccount['app_key'], ErrCode::SignErr, '签名错误'))->content());
            }
        }
        $request->offsetSet('open_account', $openAccount);
        return $next($request);
    }

    private static function res(string $appKey, string $code, string $msg, array $resBiz): array
    {
        $response = [
            'code'         => $code,
            'msg'          => $msg,
            'res_biz_json' => json_encode($resBiz),
            'nonce_str'    => md5(time()),
        ];
        $response['sign'] = Sign::generator($response, $appKey);
        return $response;
    }

    public static function fail(string $appKey, int $code, string $msg, array $resBiz = []): array
    {
        return self::res($appKey, (string)$code, $msg, $resBiz);
    }

    public static function success(string $appKey, array $resBiz = [], $msg = 'ok'): array
    {
        return self::res($appKey, '0', $msg, $resBiz);
    }
}