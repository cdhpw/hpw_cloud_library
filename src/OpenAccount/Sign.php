<?php

namespace cdhpw\CloudLibrary\OpenAccount;

/**
 * Class Sign
 * @package cdhpw\CloudLibrary\OpenAccount
 */
class Sign
{
    /**
     * 生成签名
     * @param  array  $data
     * @param  string  $appKey
     * @return string
     */
    public static function generator(array $data, string $appKey): string
    {
        ksort($data);
        $str1 = '';
        foreach ($data as $key => $value) {
            if (is_array($value) || is_object($value)) {
                continue;
            }
            if ($value === "") {
                continue;
            }
            $str1 .= $key.'='.$value.'&';
        }
        $str1 .= 'key='.$appKey;
        return strtoupper(md5($str1));
    }

    /**
     * 验证签名
     * @param  array  $data
     * @param  string  $appKey
     * @param  string  $sign
     * @return bool
     */
    public static function checkSign(array $data, string $appKey, string $sign): bool
    {
        return $sign == self::generator($data, $appKey);
    }
}