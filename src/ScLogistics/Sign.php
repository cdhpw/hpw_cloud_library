<?php

namespace cdhpw\CloudLibrary\ScLogistics;

/**
 * Class Sign
 * @package cdhpw\CloudLibrary\ScLogistics
 * @author George
 * @datetime 2021/1/19 10:41
 * @annotation
 */
class Sign
{
    /**
     * @var string
     */
    private $signStr = "";
    /**
     * @var string
     */
    private $signKey = "";
    /**
     * @var string
     */
    private $signType = "sha256";

    /**
     * @param  array  $param
     * @return $this
     * @annotation
     */
    public function setParam(array $param): self
    {
        $param = $this->filterParam($param);
        ksort($param);
        $this->signStr = '';
        foreach ($param as $key => $value) {
            $this->signStr .= $key.'='.$value.'&';
        }
        $this->signStr = substr($this->signStr, 0, -1);
        return $this;
    }

    /**
     * @param  string  $signKey
     * @return $this
     * @annotation
     */
    public function setSignKey(string $signKey): self
    {
        $this->signKey = $signKey;
        return $this;
    }

    /**
     * @param  string  $signType
     * @return $this
     * @annotation
     */
    public function setSignType(string $signType): self
    {
        $this->signKey = $signType;
        return $this;
    }

    /**
     * @return string
     * @annotation
     */
    public function build(): string
    {
        return base64_encode(hash_hmac($this->signType, $this->signStr, $this->signKey, true));
    }

    /**
     * @param  array  $param
     * @return array
     * @annotation
     */
    private function filterParam(array $param): array
    {
        unset($param['sign']);
        return array_filter($param);
    }
}