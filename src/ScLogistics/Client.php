<?php

namespace cdhpw\CloudLibrary\ScLogistics;

use Exception;

/**
 * Class Client
 * @package cdhpw\CloudLibrary\ScLogistics
 */
class Client
{
    /**
     * @var string
     */
    protected $host;
    /**
     * @var string
     */
    protected $apiKey;

    /**
     * Client constructor.
     * @param  string  $host
     * @param  string  $key
     */
    public function __construct(string $host, string $key)
    {
        $this->host = $host;
        $this->apiKey = $key;
    }

    /**
     * @param  string  $api
     * @param  array  $params
     * @return array
     * @throws Exception
     */
    public function get(string $api, array $params = []): array
    {
        $params['timestamp'] = time();
        $params['random'] = mt_rand(10000, 99999);
        $sign = new Sign();
        $signRes = $sign->setParam($params)
            ->setSignKey($this->apiKey)->build();
        $headers = [
            "sign: ".$signRes,
        ];
        $paramsStr = http_build_query($params);
        if (strpos($api, '?') === false) {
            $api .= '?'.$paramsStr;
        } else {
            $api .= '&'.$paramsStr;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->host.'/'.ltrim($api, '/'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 8000);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 30000);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $response = curl_exec($ch);
        $resInfo = curl_getinfo($ch);
        curl_close($ch);
        if ($resInfo["http_code"] != 200) {
            throw new Exception("response status code is not valid. status code: ".$resInfo["http_code"]);
        }
        $resData = json_decode($response, true);
        if (is_array($resData)) {
            return (array)$resData;
        }
        throw new Exception('can not parse:'.$response);
    }

    /**
     * @param  string  $api
     * @param  array  $params
     * @return array
     * @throws Exception
     */
    public function post(string $api, array $params = []): array
    {
        $params['timestamp'] = time();
        $params['random'] = mt_rand(10000, 99999);
        $sign = new Sign();
        $signRes = $sign->setParam($params)
            ->setSignKey($this->apiKey)
            ->build();
        $headers = [
            "sign: ".$signRes,
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->host.'/'.ltrim($api, '/'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 8000);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 30000);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $response = curl_exec($ch);
        $resInfo = curl_getinfo($ch);
        curl_close($ch);
        if ($resInfo["http_code"] != 200) {
            throw new Exception("response status code is not valid. status code: ".$resInfo["http_code"]);
        }
        $resData = json_decode($response, true);
        if (is_array($resData)) {
            return (array)$resData;
        }
        throw new Exception('can not parse:'.$response);
    }

    /**
     * @param  string  $host
     * @param  string  $key
     * @return Client
     */
    public static function newClient(string $host, string $key)
    {
        return new self($host, $key);
    }
}