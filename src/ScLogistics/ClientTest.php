<?php

namespace cdhpw\CloudLibrary\ScLogistics;

use PHPUnit\Framework\TestCase;

require __DIR__."/../../vendor/autoload.php";

class ClientTest extends TestCase
{
    public function testGet()
    {
        $cli = Client::newClient('http://demo.teseed.com.cn/paihao/public/index.php', '123');
        $a = $cli->get('/api/region', [
            'freezer_code' => '1011123',
            'b'            => '1'
        ]);
        $this->assertIsArray($a);
    }

    public function testPost()
    {
        $cli = Client::newClient('http://demo.teseed.com.cn/paihao/public/index.php', '123');
        $a = $cli->post('/api/transportation/upDz', [
            'freezer_code' => '1011123',
            'b'            => '1'
        ]);
        var_dump($a);
        $this->assertIsArray($a);
    }
}

