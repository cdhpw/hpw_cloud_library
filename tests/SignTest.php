<?php

require __DIR__."/../vendor/autoload.php";

use cdhpw\CloudLibrary\ScLogistics\Sign;
use PHPUnit\Framework\TestCase;

class SignTest extends TestCase
{
    public function testBuild()
    {
        $param = [
            'number'    => '川A12345',
            'random'    => '23818',
            'timestamp' => '1615527722'
        ];

        $key = '3192e325d463bad66160a6ce41869b3f';

        $sign = new Sign();
        $signRes = $sign->setParam($param)
            ->setSignKey($key)->build();
        $this->assertSame('UQmjMY3iD/AsDVCKID+nEsy+/pa9RdW/X3H32XSCtpo=', $signRes);
    }
}

