<?php

namespace OpenAccount;

require __DIR__."/../../vendor/autoload.php";

use cdhpw\CloudLibrary\OpenAccount\Sign;
use PHPUnit\Framework\TestCase;

class SignTest extends TestCase
{
    public function testBuild()
    {
        $param = [
            'appid'        => 'icbc54156656546454',
            'nonce_str'    => '59fb775b4eaa02541991b848aae53006',
            'req_biz_json' => '{"org_id":1310,"keywords":"1013672"}'
        ];

        $key = '3081134760dc74e65eb3871e9cb4d5fb';

        $sign = Sign::generator($param, $key);
        $bool = Sign::checkSign($param, $key, $sign);
        var_dump($sign, $bool);
        $this->assertEquals(true, $bool);
    }
}

